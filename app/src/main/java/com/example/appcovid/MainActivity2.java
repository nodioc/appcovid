package com.example.appcovid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity2 extends AppCompatActivity {
    private Button btnTemperatura;
    private  Button btnConfig;
    private  Button btnConversor;
    private Button btnCerrarSesion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        btnTemperatura = findViewById(R.id.btnTemperatura);
        btnConfig = findViewById(R.id.btnConfig);
        btnConversor = findViewById(R.id.btnConversor);
        btnCerrarSesion= findViewById(R.id.btnCerrarSesion);

        btnConversor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity2.this, MainActivityConversor.class);
                startActivity(i);
            }
        });
        btnTemperatura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity2.this, MainActivityTemperatura.class);
                startActivity(i);
            }
        });
        btnConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity2.this, MainActivityConfig.class);
                startActivity(i);
            }
        });
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity2.this, MainActivity.class);
                startActivity(i);
                Toast.makeText(MainActivity2.this, "Has vuelto al inicio", Toast.LENGTH_SHORT).show();
            }
        });


    }
}