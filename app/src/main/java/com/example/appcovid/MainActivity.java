package com.example.appcovid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText etNombre;
    private EditText etPass;
    private Button btnEntrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etNombre= findViewById(R.id.etNombre);
        etPass= findViewById(R.id.etPass);
        btnEntrar=findViewById(R.id.btnEntrar);
       btnEntrar.setOnClickListener(marioListener);

    }
    private View.OnClickListener marioListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(etNombre.getText().toString().equals("admin")&&etPass.getText().toString().equals("admin")){
            Intent i = new Intent(MainActivity.this, MainActivity2.class);
            startActivity(i);
            }else{
                Toast.makeText(MainActivity.this, "Usuario o Contraseña incorrectos", Toast.LENGTH_SHORT).show();
            }
        }
    };


}