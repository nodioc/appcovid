package com.example.appcovid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivityConversor extends AppCompatActivity {
    private EditText etCelsius1;
    private EditText etCelsius2;
    private TextView tvSolucion1;
    private EditText etFarenheit2;
    private Button btnCalcular;
    private Button btnCalcular2;
    private int grados;
    private String gradosText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_conversor);

        etCelsius1 = findViewById(R.id.etCelsius1);
        etCelsius2 = findViewById(R.id.etCelsius2);
        tvSolucion1 = findViewById(R.id.tvSolucion1);
        etFarenheit2 = findViewById(R.id.etFarenheit2);
        btnCalcular =findViewById(R.id.btnCalcular);
        btnCalcular2=findViewById(R.id.btnCalcular2);
        btnCalcular.setOnClickListener(marioListener);
        btnCalcular2.setOnClickListener(marioListener2);

    }
    private View.OnClickListener marioListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try{
                gradosText = etCelsius1.getText().toString();
                grados =Integer.parseInt(gradosText)*9/5+32;
                gradosText=grados+"";
                tvSolucion1.setText(gradosText);
            }catch(NumberFormatException e){
                    e.printStackTrace();
            }
        }
    };
    private View.OnClickListener marioListener2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try{
                gradosText = etFarenheit2.getText().toString();
                grados =Integer.parseInt(gradosText)*5/9-32;
                gradosText=grados+"";
                etCelsius2.setText(gradosText);
            }catch(NumberFormatException e){
                e.printStackTrace();
            }
        }
    };


}