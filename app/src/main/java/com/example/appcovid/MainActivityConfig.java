package com.example.appcovid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class MainActivityConfig extends AppCompatActivity {
    private Button btnGuardarConf;
    private SharedPreferences sp;
    private RadioButton rbCelsius;
    private RadioButton rbFarenheit;
    private RadioGroup rgGrados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_config);
        btnGuardarConf=findViewById(R.id.btnGuardarConf);
        rbCelsius=findViewById(R.id.rbCelsius);
        rbFarenheit=findViewById(R.id.rbFarenheit);
        rgGrados=findViewById(R.id.rgGrados);

        sp=getSharedPreferences("TemperaturaDatos",MODE_PRIVATE);
        rbCelsius.setChecked(sp.getBoolean("Celsius",false));
        rbFarenheit.setChecked(sp.getBoolean("Farenheit",false));

        btnGuardarConf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor marioEditor=sp.edit();
                marioEditor.putBoolean("Celsius",rbCelsius.isChecked());
                marioEditor.putBoolean("Farenheit",rbFarenheit.isChecked());
                marioEditor.commit();
                Intent i = new Intent(MainActivityConfig.this,MainActivity2.class);
                startActivity(i);


            }
        });
    }

}