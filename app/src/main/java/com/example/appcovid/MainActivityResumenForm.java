package com.example.appcovid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class MainActivityResumenForm extends AppCompatActivity {
    private TextView tvNombre2;
    private TextView tvApellidos2;
    private TextView tvCiudad2;
    private TextView tvTemperatura2;
    private TextView tvProvincia2;
    private SharedPreferences sp;
    private Button btnColor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_resumen_form);
        tvNombre2= findViewById(R.id.tvNombre2);
        tvApellidos2= findViewById(R.id.tvApellidos2);
        tvCiudad2= findViewById(R.id.tvCiudad2);
        tvTemperatura2=findViewById(R.id.tvTemperatura2);
        tvProvincia2=findViewById(R.id.tvProvincia2);
        btnColor=findViewById(R.id.btnColor);
        //Shared Preferences
        sp=getSharedPreferences("TemperaturaDatos",MODE_PRIVATE);

        tvNombre2.setText(sp.getString("Nombre", ""));
        tvApellidos2.setText(sp.getString("Apellidos", ""));
        tvCiudad2.setText(sp.getString("Ciudad", ""));
        tvTemperatura2.setText(sp.getInt("Temperatura", 0)+"");
        tvProvincia2.setText(sp.getString("Grados", ""));

        if(sp.getInt("Temperatura", 0)>38 && sp.getString("Grados","")=="Celsius"){
                btnColor.setBackgroundColor(Color.parseColor("#FF0000"));
        }
        if(sp.getInt("Temperatura", 0)>100 && sp.getString("Grados","")=="Farenheit"){
            btnColor.setBackgroundColor(Color.parseColor("#FF0000"));
        }






    }
}