package com.example.appcovid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivityTemperatura extends AppCompatActivity {
    private SharedPreferences sp;
    private Button btnFinal;
    private TextView tvName;
    private TextView tvApellidos;
    private TextView tvCiudad;
    private TextView tvTemperatura;
    private TextView tvProvincia;
    private Switch swF;
    private  Switch swC;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_temperatura);
        sp=getSharedPreferences("TemperaturaDatos",MODE_PRIVATE);
        btnFinal= findViewById(R.id.btnFinal);
        btnFinal.setOnClickListener(marioListener);
        tvName= findViewById(R.id.tvName);
        tvApellidos= findViewById(R.id.tvApellidos2);
        tvCiudad= findViewById(R.id.tvCiudad2);
        tvTemperatura=findViewById(R.id.tvTemperatura2);
        tvProvincia=findViewById(R.id.tvProvincia2);
        swC=findViewById(R.id.swC);
        swF=findViewById(R.id.swF);
        swF.setChecked(sp.getBoolean("Farenheit",false));
        swC.setChecked(sp.getBoolean("Celsius",false));
       // btnFinal.setOnClickListener(marioBotonFin);
       // sp=getSharedPreferences("TemperaturaDatos",MODE_PRIVATE);
    }



    private View.OnClickListener marioListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                SharedPreferences.Editor marioEditor=sp.edit();
                marioEditor.putString("Nombre",tvName.getText().toString());
                marioEditor.putString("Apellidos",tvApellidos.getText().toString());
                marioEditor.putString("Ciudad",tvCiudad.getText().toString());
                marioEditor.putString("Provincia",tvProvincia.getText().toString());
                marioEditor.putInt("Temperatura",Integer.parseInt(tvTemperatura.getText().toString()));

                if(swC.isChecked()){
                    marioEditor.putString("Grados","Celsius");
                }else if(swF.isChecked()) {
                marioEditor.putString("Grados","Farenheit");
                }
                marioEditor.commit();
                Intent i = new Intent(MainActivityTemperatura.this,MainActivityResumenForm.class);
                startActivity(i);
        }
    };
}